"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "roles",
      [
        {
          role: "Software Developer",
          updatedAt: "2022-02-09T08:36:44.621Z",
          createdAt: "2022-02-09T08:36:44.621Z",
        },
        {
          role: "Software Engineer",
          updatedAt: "2022-02-09T08:36:44.621Z",
          createdAt: "2022-02-09T08:36:44.621Z",
        },
        {
          role: "Software Tester",
          updatedAt: "2022-02-09T08:36:44.621Z",
          createdAt: "2022-02-09T08:36:44.621Z",
        },
        {
          role: "Software Development Engineer",
          updatedAt: "2022-02-09T08:36:44.621Z",
          createdAt: "2022-02-09T08:36:44.621Z",
        },
        {
          role: "Associate Software Developer",
          updatedAt: "2022-02-09T08:36:44.621Z",
          createdAt: "2022-02-09T08:36:44.621Z",
        }
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
