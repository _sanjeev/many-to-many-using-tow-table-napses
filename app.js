const express = require("express");
const { sequelize, User, Role } = require("./models");

const app = express();
app.use(express.json());

app.post("/users", async (req, res) => {
  const { name, email, number } = req.body;
  try {
    const user = await User.create({ name, email, number });
    return res.json(user);
  } catch (error) {
    return res.json(error);
  }
});

// app.post('/roles', async (req, res) => {
//   const {role} = req.body;
//   try {
//     const roles = await Role.create({role});
//     return res.json(roles);
//   } catch (error) {
//     return res.json(error);
//   }
// })

// app.post("/users/:id/roles/:roleId", async (req, res) => {
//   const id = req.params.id;
//   const {role} = req.body;

//   try {
//     const user = await User.findOne({
//       where: {
//         id,
//       },
//     });
//     const roles = await Role.create({role});
//     // await user.addRole(roles, { through: { selfGranted: false } });
//     return res.json(roles);
//   } catch (error) {
//     return res.json(error);
//   }
// });

// app.post("/users/:userId/roles", async (req, res) => {
//   const userId = req.params.userId;
//   const {role} = req.body;
//   try {
//     const user = await User.findOne({
//       where: {
//         id: userId,
//       },
//     });
//     const roles = await Role.create({role});
//     await user.addRole(roles, { through: { selfGranted: false } });
//     return res.json(roles);
//   } catch (error) {
//     return res.json(error);
//   }
// });

app.post("/users/:userId/roles/:roleId", async (req, res) => {
  const userId = req.params.userId;
  const roleId = req.params.roleId;
  try {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });
    const roles = await Role.findOne({
      where: {
        id: roleId,
      },
    });
    await user.addRole(roles, { through: { selfGranted: false } });
    return res.json(roles);
  } catch (error) {
    return res.json(error);
  }
});

app.put("/users/:userId/roles/:roleId", async (req, res) => {
  const userId = req.params.userId;
  const roleId = req.params.roleId;
  const {role} = req.body;
  try {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });
    const roles = await Role.findOne({
      where: {
        id: roleId,
      },
    });
    roles.role = role;
    await roles.save();
    await user.addRole(roles, { through: { selfGranted: false } });
    return res.json(roles);
  } catch (error) {
    return res.json(error);
  }
});

app.delete("/users/:userId/roles/:roleId", async (req, res) => {
  const userId = req.params.userId;
  const roleId = req.params.roleId;
  try {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });
    const roles = await Role.findOne({
      where: {
        id: roleId,
      },
    });
    // roles.role = role;
    await roles.destroy();
    // await user.addRole(roles, { through: { selfGranted: false } });
    return res.json({message: "roles deleted"});
  } catch (error) {
    return res.json(error);
  }
});

app.post("/roles/:id/users", async (req, res) => {
  const id = req.params.id;
  const { name, email, number } = req.body;
  try {
    const roles = await Role.findOne({
      where: {
        id,
      },
    });
    const user = await User.create({ name, email, number });
    await roles.addUser(user, { through: { selfGranted: false } });
    return res.json(user);
  } catch (error) {
    return res.json(error);
  }
});

app.get("/roles/:id/users", async (req, res) => {
  const id = req.params.id;
  try {
    const roles = await Role.findOne({
      where: {
        id,
      },
      include: [User],
    });
    return res.json(roles);
  } catch (error) {
    return res.json(error);
  }
});

app.get("/users/:id/roles", async (req, res) => {
  const id = req.params.id;
  try {
    const user = await User.findOne({
      where: {
        id,
      },
      include: [Role],
    });
    // const roles = await Role.create({ role });
    // await user.addRole(Role, { through: { selfGranted: false } });
    return res.json(user);
  } catch (error) {
    return res.json(error);
  }
});

sequelize.sync({ alter: true }).then(() => {
  app.listen({ port: 4000 }, async () => {
    console.log("Server hosted on port 4000");
    await sequelize.authenticate();

    console.log("Databse Connected");
  });
});
